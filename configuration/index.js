
const URL = {
    getForum: "https://h.nimingban.com/Api/getForumList",
    getThreadsForForumID: function (forumID, pageNumber) {
        return "https://h.nimingban.com/Api/showf/id/" + forumID + "/page/" + pageNumber;
    },
    getRepliesForThreadID: function (threadID, pageNumber) {
        return "https://h.nimingban.com/Api/thread/id/" + threadID + "/page/" + pageNumber;
    },
    getQuoteForThreadID: function (threadID) {
        return "https://h.nimingban.com/Api/ref/id/" + threadID;
    },
    postToForumID: function () {
        return "https://h.nimingban.com/Home/Forum/doPostThread.html"
    },
    postToThreadID: function () {
        return "https://h.nimingban.com/Home/Forum/doReplyThread.html"
    },
    aDaoHotContentServer: function () {
        return "mongodb://ADaoManager:6o4-BbB-nUT-9a3@ds062339.mlab.com:62339/adao_hot_content"
    }
};

module.exports = URL;