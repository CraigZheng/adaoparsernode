const mongoose = require('mongoose');
const util = require('util');
const configuration = require('../configuration');
const allForumsResponseModel = require('../models/AllForumsResponse').mongooseModel();

class ExpiredCleaner {
    constructor(expiryDate) {
        this.expiryDate = expiryDate;
    }

    clean() {
        var self = this;
        if (this.expiryDate) {
            mongoose.connect(configuration.aDaoHotContentServer(), function (error) {
                // Find documents that are older than expiry date, and remove them.
                allForumsResponseModel.find({ retrievalDate: { $lt: self.expiryDate } }, function (error, documents) {
                    if (error) {
                        util.log(error);
                    } else if (documents.length) {
                        // Remove all matching documents.
                        documents.remove();
                    }
                });
            });
        }
    }
}

module.exports = ExpiredCleaner;