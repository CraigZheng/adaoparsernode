/**
 * This controller fetches a new copy of all forums, and update the mongoDB with it.
*/

const mongoose = require('mongoose');
const mongoURL = require('../configuration').aDaoHotContentServer();
const ThreadsDownloader = require('../controller/ThreadsDownloader');
const ADaoForum = require('../models/ADaoForum').ADaoForum;
const ForumModel = ADaoForum.mongooseModel();
const util = require('util');

class ForumsFetcher {
    /**
     * Fetch the latest forums and upload them to the server.
     * @param {function} callback a function with parameters ([error], forums).
     */
    updateForums(callback) {
        // Capture fetchForums function.
        var fetchForums = this.fetchForums;
        mongoose.connect(mongoURL, function (error) {
            if (error) {
                util.log(error);
                if (callback) {
                    callback([error], null);
                }
            } else {
                // Download a fresh copy of forums.
                fetchForums(true, function (error, forums) {
                    if (error != null) {
                        util.log(error);
                        if (callback) {
                            callback([error], null);
                        }
                        return;
                    }
                    // For each individual forum, create a new model, then either save or update them to the database.
                    var numberOfCompletedOperation = 0;
                    var errors = new Array();
                    forums.forEach(function (forum) {
                        var dbForum = ForumModel({
                            forumID: forum.forumID,
                            fgroup: forum.fgroup,
                            sort: forum.sort,
                            name: forum.name,
                            showName: forum.showName,
                            message: forum.message,
                            interval: forum.interval,
                            createdDate: forum.createdDate || Date(),
                            updatedDate: forum.updatedDate || Date(),
                            status: forum.status
                        });
                        // Upsert operation - the actual findOneAndUpdate is not working, I am not familiar with that API.
                        ForumModel.find({ 'forumID': dbForum.forumID }, function (error, response) {
                            if (error) util.log(error);
                            if (response.length) {
                                ForumModel.remove({ 'forumID': dbForum.forumID }, function (error) {
                                    dbForum.save(function (error) {
                                        if (error) {
                                            errors.push(error);
                                            util.log(error);
                                        }
                                        numberOfCompletedOperation += 1;
                                        if (numberOfCompletedOperation >= forums.length && callback) {
                                            callback(errors, forums);
                                        }
                                    });
                                });
                            } else {
                                dbForum.save(function (error) {
                                    if (error) {
                                        errors.push(error);
                                        util.log(error);
                                    }
                                    numberOfCompletedOperation += 1;
                                    if (numberOfCompletedOperation >= forums.length && callback) {
                                        callback(errors, forums);
                                    }
                                });
                            }
                        });
                    });
                });
            }
        });
    }

    /**
     * 
     * @param {boolean} fromSouce a boolean indicate whether or not the forums should be fetched from original source. 
     * If false, the forums would be fetched from mongoDB instead.
     * @param {function} callback a function with parameters (error, forums), must be presented.
     */
    fetchForums(fromSouce, callback) {
        if (!callback) {
            util.log('No callback is provided, the forums would not be fetched.')
            return;
        }
        if (fromSouce) {
            new ThreadsDownloader().getForums(function (error, response, forumGroups) {
                if (error) {
                    util.log(error);
                    callback(error, null);
                    return;
                }
                var forums = Array();
                forumGroups.forEach(function (forumGroup) {
                    forumGroup.forums.forEach(function (forum) {
                        forums.push(forum);
                    });
                });
                callback(null, forums);
            });
        } else {
            mongoose.connect(mongoURL, function (error) {
                if (error) {
                    util.log(error);
                    if (callback) callback(error, null);
                    return;
                }
                ForumModel.find({}, function (error, documents) {
                    var forums = Array();
                    documents.forEach(function (document) {
                        forums.push(new ADaoForum(document.toJSON()));
                    });
                    callback(error, forums);
                });
            });
        }
    }
}

module.exports = ForumsFetcher;