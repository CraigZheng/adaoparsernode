const mongoose = require('mongoose');
const util = require('util');
const AllForumsResponse = require('../models/AllForumsResponse');
const allForumsResponseModel = AllForumsResponse.mongooseModel(mongoose);

class HotContentGenerator {

    update(callback) {
        mongoose.connect(require('../configuration').aDaoHotContentServer(), function (error) {
            if (error) {
                util.log(error);
            } else {
                allForumsResponseModel.find({}, function (error, documents) {
                    if (error) {
                        util.log(error);
                    } else {
                        // Generate a json file with the returned documents.
                        const allForumsResponses = documents.map(function (document) {
                            return new AllForumsResponse(document.toJSON());
                        });
                        // Figure out the forums with most responses, the threads with most responses,
                        // and potentially more...
                        var forums = Array();
                        // Counting from the earliest to the latest.
                        var accumulatedThreads = {};
                        allForumsResponses.reverse().forEach(function (allForumsResponse) {
                            allForumsResponse.responses.forEach(function (forumResponse) {
                                forumResponse.threads[0].threads.forEach(function (thread) {
                                    // Create an array in accumulatedThreads with the thread ID.
                                    const threadID = thread.threadID;
                                    if (accumulatedThreads[`${threadID}`] == undefined) {
                                        accumulatedThreads[`${threadID}`] = Array();
                                    }
                                    // Push this thread to the accumulatedThreads with the thread ID.
                                    accumulatedThreads[`${threadID}`].push(thread);
                                });
                            });
                        });
                        util.log(accumulatedThreads.length);
                    }
                }).limit(5); // Get the last 5 documents.
            }
        });
    }

}



module.exports = HotContentGenerator;