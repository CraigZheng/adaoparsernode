const threadsFetcher = new (require('./ThreadsFetcher'))();
const forumsFetcher = new ((require('./ForumsFetcher')))();
const expiredCleaner = new ((require('./ExpiredCleaner')))();
const util = require('util');

class PeriodicUpdater {

    start() {
        // Immediately update the forums.
        forumsFetcher.updateForums(function (errors, forums) {
            if (errors.length) util.log(errors);
            util.log(`Forums update completed with ${forums.length} elements downloaded.`);
            threadsFetcher.forums = forums;
        });

        setTimeout(function () {
            threadsFetcher.updateResponsesForAllForums(function (errors, threads) {
                if (errors && errors.length) util.log(errors);
                util.log(`Threads fetch operation completed.`)
            });
        }, 30000); // After 30 seconds, perform the first threads fetch operation.

        // Schedule updates in the future.
        this.forumsFetcherInterval = setInterval(function () {
            forumsFetcher.updateForums(function (errors, forums) {
                util.log(`Forums update completed with ${forums.length} elements downloaded.`);
                if (forums.length) {
                    threadsFetcher.forums = forums;
                }
            });
        }, 3600 * 24 * 1000); // One fetch operation per day.

        this.threadsFetcherInterval = setInterval(function () {
            threadsFetcher.updateResponsesForAllForums(function (errors, threads) {
                if (errors) util.log(errors);
                util.log(`Threads fetch operation completed.`)
            });
        }, 3600 * 1000); // One fetch per hour.

        // Immediately fireup an expiryCleaner session.
        expiredCleaner.expiryDate = new Date();
        expiredCleaner.expiryDate.setMonth(expiredCleaner.expiryDate.getMonth() - 1);
        expiredCleaner.clean();
        // Schedule clean up, once per day, check for documents that are older than 1 month and delete them.
        this.cleanupInterval = setInterval(function () {
            var expiryDate = new Date();
            expiryDate.setMonth(expiryDate.getMonth() - 1);
            expiredCleaner.expiryDate = expiryDate;
            expiredCleaner.clean();
        }, 24 * 3600 * 1000);
    }

    stop() {
        clearInterval(this.threadsFetcherInterval);
        clearInterval(this.forumsFetcherInterval);
    }
}

module.exports = PeriodicUpdater;