const utl = require('util');
const request = require('request');
const configuration = require('../configuration');
const ADaoThread = require('../models/ADaoThread');
const FileSystem = require('fs');

class ThreadPoster {

    constructor(cookies) {
        this.cookies = cookies;
    }

    postThread(options, callback) {
        if (typeof options === "object") {
            var targetURL = options.forumID ? configuration.postToForumID() : configuration.postToThreadID()
            var formData = this.getFormDataWithThread(options.thread, options.imageFilePath);
            options.forumID ? formData.fid = `${options.forumID}` : formData.resto = `${options.threadID}`;
            var jar = request.jar();
            if (this.cookies != null) {
                jar.setCookie(this.cookies, targetURL);
            }

            request.post(targetURL, {
                headers: {
                    'User-Agent': 'ADaoParserNode'
                },
                formData: formData,
                jar: jar
            }, function (error, response, body) {
                if (callback != null) callback(error, response, body, jar.getCookies(targetURL))
            });
        } else {
            if (callback != null) {
                callback(false);
            }
        }
    }

    getFormDataWithThread(thread, imageFilePath) {
        if (typeof thread !== "object") return null
        var formData = {
            name: thread.name,
            title: thread.title,
            email: thread.email,
            content: thread.content,
        };
        if (imageFilePath != undefined) {
            formData.image = FileSystem.createReadStream(imageFilePath),
            formData.water = "false"
        }
        return formData;
    }
}

var thread = new ADaoThread();
thread.name = "";
thread.title = "";
thread.content = "又需要电击了";
thread.email = "sage";
// TODO: test image.
new ThreadPoster(request.cookie("userhash=-%B0%B5%22%27%84%C3%F5%06%60%29%87%01E%ADq%B2%DB%0E%1B%8D%D3%ED%B0")).postThread({ threadID: 12343795, thread: thread }, function () {

});

module.exports = ThreadPoster;