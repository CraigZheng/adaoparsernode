var request = require('request');
var util = require('util');
const configuration = require('../configuration');
const ADaoThread = require('../models/ADaoThread')
const Forum = require('../models/ADaoForum');

class ThreadDownloader {
    constructor(headers) {
        this.headers = headers;
    }
    getForums(callback) {
        request(configuration.getForum, function (error, response, body) {
            if (error) {
                util.log(error);
            }
            var forumGroups;
            try {
                if (Array.isArray(JSON.parse(body))) {
                    forumGroups = JSON.parse(body).map(function (jsonObject) {
                        return new Forum.ADaoForumGroup(jsonObject);
                    });
                }
            } catch (exception) {
                util.log(exception);
            }
            if (callback != null) callback(error, response, forumGroups);
        })
    }

    getThreadsForForumID(forumID, pageNumber, callback) {
        request(configuration.getThreadsForForumID(forumID, pageNumber), function (error, response, body) {
            if (error) {
                util.log(error);
            }
            var threads;
            try {
                threads = JSON.parse(body).map(function (jsonString) {
                    return new ADaoThread(jsonString);
                });
            } catch (exception) {
                util.log(exception);
            }
            if (callback != null) callback(error, response, threads);
        });
    };

    getRepliesForThreadID(threadID, pageNumber, callback) {
        request(configuration.getRepliesForThreadID(threadID, pageNumber), function (error, response, body) {
            if (error) {
                util.log(error);
            }
            var threads;
            try {
                threads = new ADaoThread(JSON.parse(body));
            } catch (exception) {
                util.log(exception);
            }
            if (callback != null) callback(error, response, threads);
        });
    }
};

module.exports = ThreadDownloader;