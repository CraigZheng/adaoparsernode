/**
 * This controller fetch the first few pages for each forum.
 * If no forum is available right now, it will not fetch.
 */

const mongoose = require('mongoose');
const util = require('util');
const ADaoForum = require('../models/ADaoForum');
const ADaoThread = require('../models/ADaoThread');
const AllForumsResponse = require('../models/AllForumsResponse');
const threadsDownloader = new (require('./ThreadsDownloader'))();
const forumsFetcher = new (require('./ForumsFetcher'))();

class ThreadsFetcher {
    /**
     * 
     * @param {Array} forums an array of ADaoForum objects, must not be nil.
     */
    constructor(forums) {
        if (Array.isArray(forums)) {
            this.forums = forums;
        }
    }

    /**
     * @desc This function reads the latest threads from all forums, and update mongo DB with the results.
     * @param {Function} callback function with parameters (errors, AllForumsResponse).
     */
    updateResponsesForAllForums(callback) {
        if (!(Array.isArray(this.forums) && this.forums.length)) {
            util.log("This.forums must not be null.");
            callback(null, null);
            return;
        }
        this.consumableForums = this.forums.slice();
        this.updateConsumableForums(Array(),
            Array(),
            this,
            function (errors, responses) {
                const allForumsResponse = new AllForumsResponse({
                    retrievalDate: Date(),
                    responses: responses
                });
                const responseInstance = allForumsResponse.mongooseInstance(mongoose);
                responseInstance.save(function (error) {
                    if (error) util.log(error);
                    if (callback) callback(errors, allForumsResponse);
                });
            });
    }

    /**
     * 
     * @param {Array} errorBuffer errors during the loading would accumulate in this buffer.
     * @param {Array} responseBuffer threads downloaded would accumulate in this buffer.
     * @param {Object} self an instance of ThreadsFetcher.
     * @param {Function} callback callback with parameters([errors], [ForumResponse]), must not be null.
     */
    updateConsumableForums(errorBuffer, responseBuffer, self, callback) {
        // If no more consumableForums left, return with the downloaded errors and threads.
        if (!self.consumableForums.length) {
            callback(errorBuffer, responseBuffer);
            return;
        }
        const forum = self.consumableForums.pop()
        // If the incoming forum does not have a forumID, return early.
        if (forum.forumID <= 0) {
            util.log(`${forum.name} does not have a valid forumID, calling next operation...`)
            self.updateConsumableForums(errorBuffer, responseBuffer, self, callback);
            return;
        }
        util.log(`Fetching response for ${forum.name}, ${self.consumableForums.length} forums remaining...`);
        self.fetchThreadsForForumID(forum.forumID, 2, function (errors, threads) {
            if (errors.length) {
                // TODO: what to do? Terminate?
                errorBuffer.push({
                    forum: forum,
                    errors: errors
                });
            }
            if (threads.length) {
                responseBuffer.push({
                    forum: forum,
                    threads: threads,
                    retrievalDate: Date()
                });
            }
            // Wait 5 seconds, then call updateConsumableForums() again.
            setTimeout(function () {
                self.updateConsumableForums(errorBuffer, responseBuffer, self, callback);
            }, 5000);
        });
    }

    /**
     * 
     * @param {Number} forumID 
     * @param {Number} numberOfPages 
     * @param {Function} callback a function with parameters ([error], [{page, [threads]}}]). 
     */
    fetchThreadsForForumID(forumID, numberOfPages, callback) {
        if (!callback) {
            return;
        }
        var errors = Array();
        var threads = Array();
        for (var i = 1; i <= numberOfPages; i++) {
            const pageNumber = i;
            threadsDownloader.getThreadsForForumID(forumID, pageNumber, function (error, response, returnedThreads) {
                if (error) {
                    errors.push(error);
                }
                if (returnedThreads) {
                    // Threads with the assosiacted page number.
                    threads.push({
                        page: pageNumber,
                        threads: returnedThreads
                    });
                }
                // When either there's an error or a desired number of pages have been downloaded, invoke callback.
                if (callback && (errors.length || threads.length >= numberOfPages)) {
                    callback(errors, threads)
                    // callback function is now consumed.
                    callback = null;
                }
            });
        }
    }
}

module.exports = ThreadsFetcher;