const app = require('express')();
const util = require('util');
const port = process.env.PORT || 8000
const periodicUpdater = new (require('./controller/PeriodicUpdater'))();

periodicUpdater.start();

app.listen(port, function() {
    util.log(`A dao parser node up and running on port ${port}`);
});