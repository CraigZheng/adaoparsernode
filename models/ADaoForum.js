class ADaoForumGroup {
    constructor(jsonObject) {
        if (typeof jsonObject !== "object") return null;
        this.id = jsonObject.id;
        this.sort = jsonObject.sort;
        this.name = jsonObject.name;
        this.status = jsonObject.status;
        if (Array.isArray(jsonObject.forums)) {
            this.forums = jsonObject.forums.map(function (jsonObject) {
                return new ADaoForum(jsonObject);
            });
        }
    }
}

class ADaoForum {
    constructor(jsonObject) {
        if (typeof jsonObject !== "object") return null;
        this.forumID = jsonObject.id || jsonObject.forumID;
        this.fgroup = jsonObject.fgroup;
        this.sort = jsonObject.sort;
        this.name = jsonObject.name;
        this.showName = jsonObject.showName;
        this.message = jsonObject.msg || jsonObject.message;
        this.interval = jsonObject.interval;
        this.createdDate = Date.parse(jsonObject.createdAt || jsonObject.createdDate);
        this.updatedDate = Date.parse(jsonObject.updateAt || jsonObject.updatedDate);
        this.status = jsonObject.status;
    }

    static mongooseSchema(mon) {
        const mongoose = mon || require('mongoose');
        return new mongoose.Schema({
            forumID: String,
            fgroup: String,
            sort: Boolean,
            name: String,
            showName: String,
            message: String,
            interval: String,
            createdDate: Date,
            updatedDate: Date,
            status: Boolean
        });
    }

    static mongooseModel(mon) {
        const mongoose = mon || require('mongoose');
        var model;
        try {
            model = mongoose.model('Forum')
        } catch (_) { }
        return model || mongoose.model('Forum', this.mongooseSchema(mongoose));
    }
}

module.exports = {
    ADaoForum: ADaoForum,
    ADaoForumGroup: ADaoForumGroup
}