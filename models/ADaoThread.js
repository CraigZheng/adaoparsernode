class ADaoThread {
    constructor(jsonObject) {
        if (typeof jsonObject !== "object") return null;
        this.threadID = jsonObject["id"] || jsonObject["threadID"];
        if (jsonObject["img"] != null && jsonObject["ext"] != null) {
            this.imageName = jsonObject["img"] + jsonObject["ext"];
        }
        this.postDate = jsonObject.now || jsonObject.postDate;
        this.userID = jsonObject.userid || jsonObject.userID;
        this.name = jsonObject.name;
        this.email = jsonObject.email;
        this.title = jsonObject.title;
        this.content = jsonObject.content;
        this.sage = jsonObject.sage;
        this.admin = jsonObject.admin;
        this.responseCount = jsonObject.replyCount || jsonObject.responseCount;
        var replies = jsonObject.replys || jsonObject.replies;
        if (Array.isArray(replies)) {
            this.replies = replies.map(function (jsonObject) {
                return new ADaoThread(jsonObject);
            });
        }
    }

    mongooseInstance(mon) {
        const mongoose = mon || require('mongoose');
        return ADaoThread.mongooseModel(mongoose)({
            threadID: this.threadID,
            imageName: this.imageName,
            userID: this.userID,
            name: this.name,
            email: this.email,
            title: this.title,
            content: this.content,
            sage: this.sage,
            admin: this.admin,
            responseCount: this.responseCount,
            postDate: this.postDate
        });
    }

    static mongooseSchema(mon) {
        const mongoose = mon || require('mongoose');
        return new mongoose.Schema({
            threadID: String,
            imageName: String,
            postDate: Date,
            userID: String,
            name: String,
            email: String,
            title: String,
            content: String,
            sage: Boolean,
            admin: Boolean,
            responseCount: Number
        });
    }

    static mongooseModel(mon) {
        const mongoose = mon || require('mongoose');
        var model;
        try {
            model = mongoose.model('Thread');
        } catch (_) { };
        return model || mongoose.model('Thread', this.mongooseSchema(mongoose));
    }

}

module.exports = ADaoThread;