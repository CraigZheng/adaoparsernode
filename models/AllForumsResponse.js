const ForumResponse = require('./ForumResponse');

class AllForumsResponse {
    constructor(jsonObject) {
        if (typeof jsonObject !== "object") { return null }
        this.retrievalDate = jsonObject.retrievalDate;
        // Parse the incoming jsonObject, and get the ADaoThreads from it.
        this.responses = Array();
        const self = this;
        jsonObject.responses.forEach(function (response) {
            const newResponse = new ForumResponse(response);
            self.responses.push(newResponse);
        });
    }

    static mongooseSchema(mon) {
        const mongoose = mon || require('mongoose');
        return new mongoose.Schema({
            retrievalDate: Date,
            responses: [ForumResponse.mongooseSchema(mongoose)]
        });
    }

    static mongooseModel(mon) {
        const mongoose = mon || require('mongoose');
        var model;
        try {
            model = mongoose.model('AllForumsResponse');
        } catch (_) { }
        return model || mongoose.model('AllForumsResponse', this.mongooseSchema(mongoose));
    }

    mongooseInstance(mon) {
        const mongoose = mon || require('mongoose');
        return AllForumsResponse.mongooseModel(mongoose)({
            retrievalDate: this.retrievalDate,
            responses: this.responses.map(function (response) {
                return response.mongooseInstance(mongoose);
            })
        });
    }
}

module.exports = AllForumsResponse;