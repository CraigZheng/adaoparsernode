const ADaoThread = require('./ADaoThread');
const ADaoForum = require('./ADaoForum').ADaoForum;

class ForumResponse {
    constructor(jsonObject) {
        if (typeof jsonObject !== "object") return null;
        this.forum = new ADaoForum(jsonObject.forum);
        this.threads = jsonObject.threads.map(function (dictionary) {
            return {
                page: dictionary.page,
                threads: dictionary.threads.map(function (dictionary) {
                    return new ADaoThread(dictionary);
                })
            }
        })
        this.retrievalDate = jsonObject.retrievalDate;
    }

    static mongooseSchema(mon) {
        const mongoose = mon || require('mongoose');
        return new mongoose.Schema({
            retrievalDate: Date,
            forum: ADaoForum.mongooseSchema(mongoose),
            threads: [{
                page: Number,
                threads: [ADaoThread.mongooseSchema(mongoose)]
            }]
        });
    }

    static mongooseModel(mon) {
        const mongoose = mon || require('mongoose');
        var model;
        try {
            model = mongoose.model('ForumResponse');
        } catch (_) { }
        return model || mongoose.model('ForumResponse', this.mongooseSchema(mongoose));
    }

    mongooseInstance(mon) {
        const mongoose = mon || require('mongoose');
        return ForumResponse.mongooseModel(mon)({
            retrievalDate: this.retrievalDate,
            forum: this.forum,
            threads: this.threads.map(function (thread) {
                return {
                    page: thread.page,
                    threads: thread.threads.map(function (thread) {
                        return thread.mongooseInstance(mongoose);
                    })
                };
            })
        });
    }
}

module.exports = ForumResponse;